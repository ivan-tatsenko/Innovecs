# Innovecs interview task
Please find the solution for tasks related to concurrency in the `com.innovecs.interview.task2` package

Please find the solution in the `com.innovecs.interview` package.

### Assumptions 
In the second task it is not specified that indexes `p` and `q` should be different.
The task is implemented assuming that the same value can be used to find the maximum sum.

### Validation
In case you want to check the implementation against your data please modify the test source and run 
```console
$ mvn test
```