package com.innovecs.interview;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class MaxSumTest {

    private MaxSum underTest = new MaxSum();

    @ParameterizedTest
    @MethodSource("arrayAndResult")
    void shouldCalculateMax(int[] array, MaxSumResult result) {
        MaxSumResult actual = underTest.findMaxSum(array);

        assertThat(actual).isEqualToComparingFieldByField(result);
    }

    private static Stream<Arguments> arrayAndResult() {
        return Stream.of(
                Arguments.of(new int[]{1, 2}, new MaxSumResult(4, 1, 0)),
                Arguments.of(new int[]{6, 6, 3, 7}, new MaxSumResult(16, 3, 0)),
                Arguments.of(new int[]{1, 2, 3, 2, 1}, new MaxSumResult(6, 2, 0)),
                Arguments.of(new int[]{5, 7, 3, 4, 5}, new MaxSumResult(15, 4, 1)),
                Arguments.of(new int[]{5, 7, 3, 4, 4}, new MaxSumResult(14, 1, 1))
        );
    }

}