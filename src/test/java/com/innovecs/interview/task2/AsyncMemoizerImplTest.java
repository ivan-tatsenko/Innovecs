package com.innovecs.interview.task2;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

class AsyncMemoizerImplTest {

    private AsyncMemoizerImpl<String, String> underTest = new AsyncMemoizerImpl<>();

    @Test
    public void shouldReturnValueWithMono() {
        Mono<String> result = underTest.getOrCompute("test", Mono::just);

        StepVerifier.create(result)
                    .expectNext("test")
                    .verifyComplete();
    }

    @Test
    public void shouldCallSupplierOnlyOnce() {
        @SuppressWarnings("unchecked")
        Function<String, Mono<String>> valueSupplier = mock(Function.class);
        when(valueSupplier.apply(anyString())).then(invocation -> Mono.just(invocation.getArguments()[0]));

        Mono<String> result1 = underTest.getOrCompute("test", valueSupplier);
        Mono<String> result2 = underTest.getOrCompute("test", valueSupplier);

        StepVerifier.create(result1)
                    .expectNext("test")
                    .verifyComplete();

        StepVerifier.create(result2)
                    .expectNext("test")
                    .verifyComplete();

        verify(valueSupplier, times(1)).apply("test");
    }

    @Test
    public void shouldNotStoreWhenFunctionReturnNull() {
        Mono<String> resultAbsent = underTest.getOrCompute("test", k -> null);
        assertThat(resultAbsent).isNull();

        Mono<String> resultPresent = underTest.getOrCompute("test", Mono::just);
        StepVerifier.create(resultPresent)
                    .expectNext("test")
                    .verifyComplete();
    }

    @Test
    void shouldNotStoreWhenExceptionWasThrown() {
        try {
            underTest.getOrCompute("test", k -> {
                throw new RuntimeException("Test exception");
            });
            fail("Exception should be thrown");
        } catch (RuntimeException e) {
            //expected
        }
        Mono<String> result = underTest.getOrCompute("test", Mono::just);

        StepVerifier.create(result)
                    .expectNext("test")
                    .verifyComplete();
    }

    @Test
    void shouldNotReevaluate() {
        AtomicInteger callsCount = new AtomicInteger(0);
        Function<String, String> valueSupplier = key -> {
            if (callsCount.getAndIncrement() > 0) {
                throw new RuntimeException("Should not recalculate the value");
            }
            return key;
        };

        Mono<String> result1 = underTest.getOrCompute("test", k -> Mono.defer(() -> Mono.just(valueSupplier.apply(k))));
        Mono<String> result2 = underTest.getOrCompute("test", k -> Mono.defer(() -> Mono.just(valueSupplier.apply(k))));

        StepVerifier.create(result1).expectNext("test").verifyComplete();
        StepVerifier.create(result2).expectNext("test").verifyComplete();
    }

}