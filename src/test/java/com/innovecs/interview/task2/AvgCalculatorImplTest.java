package com.innovecs.interview.task2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

class AvgCalculatorImplTest {

    private static final List<Integer> FROM_1_TO_101 = IntStream.iterate(1, l -> l + 1).limit(101).boxed().collect(toList());

    private final AvgCalculatorImpl underTest = new AvgCalculatorImpl();

    private static Stream<Arguments> avgValues() {
        return Stream.of(
                Arguments.of(emptyList(), 0),
                Arguments.of(asList(4, 6), 5),
                Arguments.of(asList(1, 2, 3, 4, 5), 3),
                Arguments.of(asList(3, 4), 3.5),
                Arguments.of(FROM_1_TO_101, 51)
        );
    }

    @Test
    void shouldReturnValueWhenNoOther() {
        underTest.update(5);

        assertThat(underTest.average()).isEqualTo(5);
    }

    @ParameterizedTest
    @MethodSource("avgValues")
    void shouldCalculateAvgWhenMultipleValues(List<Integer> values, double avg) {
        values.forEach(underTest::update);

        assertThat(underTest.average()).isEqualTo(avg);
    }

    @Test
    void shouldCalculateAvgWhenUpdateConcurrently() {
        FROM_1_TO_101.stream()
                     .parallel()
                     .forEach(underTest::update);

        assertThat(underTest.average()).isEqualTo(51);
    }

}