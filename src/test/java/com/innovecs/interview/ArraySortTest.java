package com.innovecs.interview;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArraySortTest {

    private ArraySort underTest = new ArraySort();

    @ParameterizedTest
    @MethodSource("sortableArrays")
    void shouldSortWhenArrayOf2(int[] beforeSort, int[] afterSort) {
        underTest.sort(beforeSort);

        assertThat(beforeSort).isEqualTo(afterSort);
    }

    @ParameterizedTest
    @MethodSource("notSortableArrays")
    void shouldThrowException(int[] unsortable) {
        assertThrows(IllegalArgumentException.class, () -> underTest.sort(unsortable));
    }


    private static Stream<Arguments> sortableArrays() {
        return Stream.of(
                Arguments.of(new int[]{2, 1}, new int[]{1, 2}),
                Arguments.of(new int[]{3, 10}, new int[]{3, 10}),
                Arguments.of(new int[]{2, 2, 1}, new int[]{1, 2, 2}),
                Arguments.of(new int[]{2, 2, 2, 1}, new int[]{1, 2, 2, 2}),
                Arguments.of(new int[]{2, 1, 2}, new int[]{1, 2, 2}),
                Arguments.of(new int[]{2, 2, 1, 4, 3}, new int[]{1, 2, 2, 3, 4}),
                Arguments.of(new int[]{7, 6, 6, 7, 8, 7}, new int[]{6, 6, 7, 7, 7, 8})
        );
    }

    private static Stream<int[]> notSortableArrays() {
        return Stream.of(
                new int[]{5, 3},
                new int[]{8, 7, 6},
                new int[]{7, 8, 6}
        );
    }

}