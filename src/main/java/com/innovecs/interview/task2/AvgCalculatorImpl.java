package com.innovecs.interview.task2;

import java.util.concurrent.atomic.AtomicReference;

public class AvgCalculatorImpl implements AvgCalculator {

    private AtomicReference<AvgAccumulator> avg = new AtomicReference<>(new AvgAccumulator());

    @Override
    public void update(long value) {
        avg.updateAndGet(avg -> avg.add(value));
    }

    @Override
    public double average() {
        return avg.get().avg;
    }

    private static class AvgAccumulator {
        private final long sum;
        private final long itemsCount;
        private final double avg;

        private AvgAccumulator(long sum, long itemsCount, double avg) {
            this.sum = sum;
            this.itemsCount = itemsCount;
            this.avg = avg;
        }

        private AvgAccumulator() {
            this(0, 0, 0);
        }

        private AvgAccumulator add(long item) {
            long newSum = sum + item;
            long newCount = itemsCount + 1;
            return new AvgAccumulator(newSum, newCount, ((double) newSum) / newCount);
        }
    }
}
