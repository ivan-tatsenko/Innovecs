package com.innovecs.interview.task2;

interface AvgCalculator {

    void update(long value);

    double average();

}
