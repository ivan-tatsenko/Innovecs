package com.innovecs.interview.task2;

import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class AsyncMemoizerImpl<K, V> implements AsyncMemoizer<K, V> {

    private final Map<K, Mono<V>> instances = new ConcurrentHashMap<>();

    @Override
    public Mono<V> getOrCompute(K key, Function<K, Mono<V>> valueSupplier) {
        return instances.computeIfAbsent(key, k -> prepareMono(k, valueSupplier));
    }

    private Mono<V> prepareMono(K key, Function<K, Mono<V>> valueSupplier) {
        Mono<V> result = valueSupplier.apply(key);
        return result == null ? null : result.cache();
    }

}
