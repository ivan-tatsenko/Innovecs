package com.innovecs.interview.task2;

import reactor.core.publisher.Mono;

import java.util.function.Function;

public interface AsyncMemoizer<K, V> {

    Mono<V> getOrCompute(K key, Function<K, Mono<V>> valueSupplier);
}
