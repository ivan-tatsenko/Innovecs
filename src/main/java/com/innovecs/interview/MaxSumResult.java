package com.innovecs.interview;

public class MaxSumResult {

    public final int sum;
    public final int p;
    public final int q;

    MaxSumResult(int sum, int p, int q) {
        this.sum = sum;
        this.p = p;
        this.q = q;
    }
}
