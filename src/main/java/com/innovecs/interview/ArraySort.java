package com.innovecs.interview;

public class ArraySort {

    public void sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                if (array[i] - array[i + 1] > 1) {
                    throw new IllegalArgumentException();
                }
                replace(array, i + 1);
            }
        }
    }

    private void replace(int[] array, int indexOfLower) {
        int j = indexOfLower;
        while (j > 0 && array[j - 1] > array[indexOfLower]) {
            j--;
        }
        int temp = array[j];
        array[j] = array[indexOfLower];
        array[indexOfLower] = temp;
    }
}
