package com.innovecs.interview;

public class MaxSum {

    public MaxSumResult findMaxSum(int[] array) {
        int maxP = array[0];
        int maxPIndex = 0;
        int maxQ = array[0];
        int maxQIndex = 0;
        for (int i = 1; i < array.length; i++) {
            int currP = array[i] + i;
            int currQ = array[i] - i;
            if (currP > maxP) {
                maxP = currP;
                maxPIndex = i;
            }
            if (currQ > maxQ) {
                maxQ = currQ;
                maxQIndex = i;
            }
        }
        return new MaxSumResult(maxP + maxQ, maxPIndex, maxQIndex);
    }
}

